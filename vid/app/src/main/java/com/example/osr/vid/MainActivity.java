package com.example.osr.vid;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    Button b;
    VideoView v;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

   b=(Button)findViewById(R.id.bt);
   v=(VideoView)findViewById(R.id.vv);


    }

    public void videolay(View view) {

        String path="android:resource://com.example.osr.vid/"+R.raw.abc;
        Uri uri= Uri.parse(path);
        v.setVideoURI(uri);
        v.start();
    }
}
